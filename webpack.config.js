/**
 * @nape webpack
 * @type {Object}
 * @property NoErrorsPlugin
 */

var webpack = require('webpack');

module.exports = {
	entry: {
		content: './src/app/js/content',
		background: './src/app/js/background'
	},
	output: {
		filename: '[name].js',
		libraryTarget: "var",
		// name of the global var: "Foo"
		library: "Foo"
	},
	externals: {
		// require("jquery") is external and available
		//  on the global var jQuery
		"jquery": "jQuery"
	},
	module: {
		loaders: [{
			test: /\.js/,
			exclude: '/node_modules/',
			loader: 'babel-loader'
		}]
	},
	plugins: [
		new webpack.NoErrorsPlugin(),
		new webpack.ProvidePlugin({
			jQuery: 'jquery',
			$: 'jquery',
			jquery: 'jquery'
		})
	]
};