// Background script

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
	switch (request[0].topic) {

		case 'download':
			downloadVideo({
				url: request[0].url,
				filename: request[0].filename
			});
			break;

	}

});

function downloadVideo ({url, filename}) {
	chrome.downloads.download({
		url,
		filename,
		saveAs: false
	},()=>{chrome.runtime.sendMessage({topic:'resume_download'});});
}