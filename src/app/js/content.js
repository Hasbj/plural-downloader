// Content script

/*
	global fetch, document
 */

/**
 * @name chrome
 * @type {Object}
 * @property runtime.sendMessage
 */

console.log("Downloader loaded");

let downloadButton = document.getElementById('plural-download-button');
downloadButton.addEventListener('click', getCourse, false);

var result =[];
var index = 0;
var lessonIndex =0;

function getCourse(e){
	e.preventDefault();
	if(document.getElementById('plural-download-button').classList.contains('active')){
		return undefined;
	}
	document.getElementById('plural-download-button').classList.add('active');
	chrome.tabs.executeScript({
		code: '(' + getModules + ')();'
	},loadAllModules);
}

function getModules(){
	var modules = document.getElementsByClassName('module');
	var modulesInfo = [];
	for(let i=0;i<modules.length;i++){
		modulesInfo[i] = {length:modules[i].children[1].children.length}
	}
	return {modules:modulesInfo};
}

function loadAllModules(resultInput){
	if(resultInput) result = resultInput[0].modules;
	if(index >= result.length) {
		document.getElementById('plural-download-button').classList.remove('active');
		return undefined;
	}
	chrome.tabs.executeScript({
		code: '(' + openModule + ')('+index+');'
	},()=>{
		setTimeout(loadAllVideo,200);
	});
}
function loadAllVideo(){
	if(lessonIndex>=result[index].length){
		lessonIndex = 0;
		index++;
		loadAllModules();
		return undefined;
	}
	chrome.tabs.executeScript({
		code: '(' + openVideo + ')('+index+','+lessonIndex+');'
	}, ()=>{
		setTimeout(getVideoSrc,1000);
	});
}

function openVideo(index,lessonIndex){
	document.getElementsByClassName('module')[index].children[1].children[lessonIndex].click();
}

function openModule(index){
	document.getElementsByClassName('module')[index].children[0].classList.add('open');
}

function getVideoSrc(){
	chrome.tabs.executeScript({
		code: '(' + getMediaSources + ')('+index+','+lessonIndex+');'
	}, (results) => {
		lessonIndex++;
		chrome.runtime.sendMessage(results);
	});
}

function getMediaSources (moduleIndex,index) {
	const baseFolder = "PluralSightCourses";
	let url = document.getElementsByTagName('video')[0].getAttribute('src');
	let filename = (baseFolder + "/"+document.getElementById('course-title-link').text.trim()+ "/" + moduleIndex + ' - ' +document.getElementById('module-title').innerHTML.trim() + "/" + index +' - ' +document.getElementById('clip-title').innerHTML + '.mp4');
	while(filename.indexOf(':')>=0){
		filename = filename.replace(':','');
	}
	return {
		topic: 'download',
		url,
		filename
	};
}

chrome.runtime.onMessage.addListener((request) => {
	switch (request.topic) {

		case 'resume_download':
			loadAllVideo();
			break;
	}

});